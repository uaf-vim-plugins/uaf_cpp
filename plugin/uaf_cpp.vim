function! uaf_cpp#docker_qf_fix()
    let l:x = getqflist()

    for l:line in l:x
        if l:line["bufnr"] > 0
            let l:bufname = bufname(l:line["bufnr"])
            let l:bufname = substitute(l:bufname, ".*| ", "", "")
            if match(l:bufname, "/app") == 0
                let l:newbuf = substitute(l:bufname, "/app/", "", "")
                echom l:newbuf
                let l:newbufnum = bufadd(l:newbuf)
                echom l:newbufnum
                let l:line["bufnr"] = l:newbufnum
            else
                let l:line["bufnr"] = 0
                let l:line["lnum"] = 0
            endif
        endif
    endfor
    call setqflist(l:x)
endfunction

function! uaf_cpp#compile()
    call asyncrun#run("", {'post': 'call uaf_cpp#docker_qf_fix() | :execute "cnext"'}, "docker-compose up")
endfunction

function! uaf_cpp#list_tests()
    call asyncrun#run("", {}, "docker-compose run --rm compiler ./build/test --gtest_list_tests")
endfunction

function! uaf_cpp#parse_tests()
    let l:test_list = []

    let l:x = getqflist()

    let l:suite = ""
    for l:line in l:x[2:-2]
        let l:t = l:line["text"]
        if match(l:t, "^\\S\\+\\.$") == 0
            let l:suite = substitute(l:t, "\\.", "", "")
        else
            let l:t = substitute(l:t, "^\\s\\+", "", "")
            let l:test_name = l:suite . "." . l:t
            call add(l:test_list, l:test_name)
        endif
    endfor

    return l:test_list
endfunction

function! uaf_cpp#gtest_wrapper(...)
    if a:1 == "all"
        call asyncrun#run("", {'post': 'call uaf_cpp#docker_qf_fix() | :execute "cnext"'}, "docker-compose run --rm compiler ./build/test")
    else
        call asyncrun#run("", {'post': 'call uaf_cpp#docker_qf_fix() | :execute "cnext"'}, "docker-compose run --rm compiler ./build/test --gtest_filter=" . a:1)
    endif
endfunction

function! uaf_cpp#run_test(...)
    if a:0 == 0
        call asyncrun#run("", {'post': 'call uaf_cpp#run_test(["all"] + uaf_cpp#parse_tests())'}, "docker-compose run --rm compiler ./build/test --gtest_list_tests")
    else
        call fzf#run({'source': a:1, 'sink': function('uaf_cpp#gtest_wrapper')})
    endif
endfunction
